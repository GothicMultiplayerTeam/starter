#ifndef SERVERADDRESS_H
#define SERVERADDRESS_H

#include <string>
#include <stdint.h>

class ServerAddress
{
public:
    static bool parse(std::string ip_port, ServerAddress& address);
    bool isUnassigned();

    std::string ip;
    uint16_t port;
};

#endif