#include "ServerRequest.h"

#include <Windows.h>
#include <chrono>

bool ServerRequest::init()
{
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
    {
        m_errorDescription = "Cannot initialize WinSock 2.2! Error code: ";
        m_errorDescription += WSAGetLastError();

        return false;
    }

    if ((m_socket = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET)
    {
        m_errorDescription = "Cannot create Socket!";
        return false;
    }

    u_long nMode = 1; // 1: NON-BLOCKING
    if (ioctlsocket(m_socket, FIONBIO, &nMode) == SOCKET_ERROR)
    {
        m_errorDescription = "Cannot set Socket to non-blocking state!";
        return false;
    }

    return true;
}

ServerRequest::~ServerRequest()
{
    closesocket(m_socket);
    WSACleanup();
}

bool ServerRequest::info(ServerAddress addres, Packet& receivedPacket)
{
    return send(addres, "GOi", receivedPacket);
}

bool ServerRequest::ping(ServerAddress addres, Packet& receivedPacket)
{
    return send(addres, "GOp", receivedPacket);
}

bool ServerRequest::details(ServerAddress addres, Packet& receivedPacket)
{
    return send(addres, "GOd", receivedPacket);
}

bool ServerRequest::send(ServerAddress& addres, std::string packetData, Packet& receivedPacket)
{
    struct sockaddr_in si_server;
    si_server.sin_family = AF_INET;
    si_server.sin_port = htons(addres.port);
    si_server.sin_addr.S_un.S_addr = inet_addr(addres.ip.c_str());

    sendto(m_socket, packetData.c_str(), packetData.size(), 0, (struct sockaddr*)&si_server, sizeof(si_server));

    Request request;
    request.address = addres;
    request.packet = packetData;
    request.time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    
    return listen(request, receivedPacket);
}

bool ServerRequest::listen(Request& request, Packet& receivedPacket)
{
    char buffer[512] = { 0 };

    struct sockaddr_in si_server;
    int sockLen = sizeof(si_server);

    memset((char*)&si_server, 0, sizeof(si_server));
    si_server.sin_family = AF_INET;

    for (long long now = GetTickCount64(), timeout = now + this->timeout; now <= timeout; now = GetTickCount64())
    {
        memset(buffer, 0, 512);

        int len = recvfrom(m_socket, buffer, 512, 0, (struct sockaddr*)&si_server, &sockLen);
        if (len <= 0)
        {
            Sleep(5);
            continue;
        }

        ServerAddress address;
        address.ip = inet_ntoa(si_server.sin_addr);
        address.port = ntohs(si_server.sin_port);

        // Prevent form packets that aren't response
        // for sended request
        if (memcmp(request.packet.c_str(), buffer, 3) != 0)
            return false;

        switch (buffer[2])
        {
            case 'p':
            {
                PacketPing& packet = (PacketPing&)receivedPacket;

               packet = PacketPing::parse(address, buffer, len);
                if (packet.address.isUnassigned()) continue;
                packet.ping = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - request.time;

                return true;
            }

            case 'i':
            {
                PacketInfo& packet = (PacketInfo&)receivedPacket;

                packet = PacketInfo::parse(address, buffer, len);
                if (packet.address.isUnassigned()) continue;
                packet.ping = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - request.time;

                return true;
            }

            case 'd':
            {
                PacketDetails& packet = (PacketDetails&)receivedPacket;

                packet = PacketDetails::parse(address, buffer, len);
                if (packet.address.isUnassigned()) continue;
;
                return true;
            }
        }
    }

    return false;
}