#ifndef PACKETS_H
#define PACKETS_H

#include <string>

#include "serveraddress.h"
#include "Proxy.h"

#define PACKET_VERSION 0x1

class Packet
{
public:
    virtual ~Packet() {};

    ServerAddress address;
};

class PacketPing : public Packet
{
public:
    static PacketPing parse(ServerAddress& address, char* buffer, int len);

    int ping;
};

class PacketInfo : public Packet
{
public:
    static PacketInfo parse(ServerAddress& address, char* buffer, int len);
    static PacketInfo empty(ServerAddress& address);

    int ping;
    Version version;
    std::string codeName;
    int players;
    int maxSlots;
    std::string hostName;
};

class PacketDetails : public Packet
{
public:
    static PacketDetails parse(ServerAddress& address, char* buffer, int len);

    std::string world;
    std::string description;
};

#endif
