#ifndef SERVER_REQUEST_H
#define SERVER_REQUEST_H

#include <WinSock2.h>
#pragma comment(lib, "Ws2_32.lib")

#include <string>
#include <vector>
#include <cstdint>

#include "ServerAddress.h"
#include "Packets.h"

class ServerRequest
{
private:
    struct Request
    {
        ServerAddress address;
        std::string packet;
        int64_t time = -1;

        bool isNull() { return time == -1; }
    };

public:
    bool init();
    ~ServerRequest();
    
    int timeout = 5000;
    inline std::string getErrorDescription() { return m_errorDescription; }

    bool info(ServerAddress addres, Packet& receivedPacket);
    bool ping(ServerAddress addres, Packet& receivedPacket);
    bool details(ServerAddress addres, Packet& receivedPacket);

private:
    SOCKET m_socket;
    std::string m_errorDescription = "";

    bool send(ServerAddress& addres, std::string packetData, Packet& receivedPacket);
    bool listen(Request& request, Packet& receivedPacket);
};

#endif