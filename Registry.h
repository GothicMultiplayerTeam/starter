#include <Windows.h>
#include <string>
#include <vector>

typedef unsigned long long QWORD;

namespace Registry
{
	enum class Error
	{
		OK,
		CANNOT_OPEN,
		CANNOT_WRITE,
		CANNOT_READ,
		CANNOT_READ_EXPAND,
		INCORRECT_TYPE,
	};

	class Key
	{
	private:
		Error m_errorCode;
		HKEY m_hCurrentKey;

		HKEY m_hRoot;
		std::string m_sKey;

		inline Error tryOpenWrite()
		{
			if (RegCreateKeyExA(m_hRoot, m_sKey.c_str(), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_WOW64_64KEY, NULL, &m_hCurrentKey, NULL) != ERROR_SUCCESS)
				return Error::CANNOT_OPEN;

			return Error::OK;
		}

		inline Error tryOpenRead()
		{
			if (RegOpenKeyExA(m_hRoot, m_sKey.c_str(), 0, KEY_READ | KEY_WOW64_64KEY, &m_hCurrentKey) != ERROR_SUCCESS)
				return Error::CANNOT_OPEN;

			return Error::OK;
		}

		inline void flush()
		{
			RegFlushKey(m_hCurrentKey);
		}

		inline void close()
		{
			RegCloseKey(m_hCurrentKey);
		}

		inline Error trySet(std::string valueName, DWORD type, void *value, DWORD size)
		{
			if (RegSetValueExA(m_hCurrentKey, valueName.c_str(), 0, type, reinterpret_cast<const BYTE*>(value), size) != ERROR_SUCCESS)
			{
				RegCloseKey(m_hCurrentKey);
				return Error::CANNOT_WRITE;
			}

			return Error::OK;
		}

		inline Error tryQuery(std::string valueName, DWORD *type, void *value, DWORD *size)
		{
			if (RegQueryValueExA(m_hCurrentKey, valueName.c_str(), NULL, type, reinterpret_cast<LPBYTE>(value), size) != ERROR_SUCCESS)
			{
				RegCloseKey(m_hCurrentKey);
				return Error::CANNOT_READ;
			}

			return Error::OK;
		}

	public:
		Key(HKEY root, std::string key)
		{
			m_errorCode = Error::OK;

			m_hRoot = root;
			m_sKey = std::move(key);
		}

		inline Error getErrorCode()
		{
			return m_errorCode;
		}

		inline std::string getErrorDescription()
		{
			switch (m_errorCode)
			{
				case Error::CANNOT_OPEN:
					return "Cannot open registry key";

				case Error::CANNOT_WRITE:
					return "Cannot write registry entry";

				case Error::CANNOT_READ:
					return "Cannot read registry entry";

				case Error::CANNOT_READ_EXPAND:
					return "Cannot expand registry entry during read";

				case Error::INCORRECT_TYPE:
					return "Using incorrect type with registry entry";

				default:
					return "";
			}
		}

		HKEY getRoot()
		{
			return m_hRoot;
		}

		void setRoot(HKEY root)
		{
			m_hRoot = root;
		}

		std::string getKey()
		{
			return m_sKey;
		}

		void setKey(std::string key)
		{
			m_sKey = std::move(key);
		}

		bool writeBinary(std::string valueName, std::vector<UCHAR> values)
		{
			m_errorCode = tryOpenWrite();

			if (m_errorCode != Error::OK)
				return false;

			m_errorCode = trySet(valueName, REG_BINARY, values.data(), values.size());

			if (m_errorCode != Error::OK)
				return false;

			flush();
			close();

			return true;
		}

		bool writeDWORD(std::string valueName, DWORD value)
		{
			m_errorCode = tryOpenWrite();

			if (m_errorCode != Error::OK)
				return false;

			m_errorCode = trySet(valueName, REG_DWORD, &value, sizeof(DWORD));

			if (m_errorCode != Error::OK)
				return false;

			flush();
			close();

			return true;
		}

		bool writeQWORD(std::string valueName, QWORD value)
		{
			m_errorCode = tryOpenWrite();

			if (m_errorCode != Error::OK)
				return false;

			m_errorCode = trySet(valueName, REG_QWORD, &value, sizeof(QWORD));

			if (m_errorCode != Error::OK)
				return false;

			flush();
			close();

			return true;
		}

		bool writeSZ(std::string valueName, std::string value)
		{
			m_errorCode = tryOpenWrite();

			if (m_errorCode != Error::OK)
				return false;

			m_errorCode = trySet(valueName, REG_SZ, &value[0], value.size());

			if (m_errorCode != Error::OK)
				return false;

			flush();
			close();

			return true;
		}

		bool writeExpandSZ(std::string valueName, std::string value)
		{
			m_errorCode = tryOpenWrite();

			if (m_errorCode != Error::OK)
				return false;

			m_errorCode = trySet(valueName, REG_EXPAND_SZ, &value[0], value.size());

			if (m_errorCode != Error::OK)
				return false;

			flush();
			close();

			return true;
		}

		bool writeMultiSZ(std::string valueName, std::vector<std::string> values)
		{
			m_errorCode = tryOpenWrite();

			if (m_errorCode != Error::OK)
				return false;

			std::string tmp = values[0];

			for (unsigned i = 1, end = values.size(); i != end; ++i)
				tmp += '\0' + values[i];

			m_errorCode = trySet(valueName, REG_MULTI_SZ, &tmp[0], tmp.size());

			if (m_errorCode != Error::OK)
				return false;

			flush();
			close();

			return true;
		}

		bool readBinary(std::string valueName, std::vector<BYTE> & result)
		{
			m_errorCode = tryOpenRead();

			if (m_errorCode != Error::OK)
				return false;

			DWORD type;
			DWORD size;

			m_errorCode = tryQuery(valueName, &type, NULL, &size);

			if (m_errorCode != Error::OK)
				return false;

			if (type != REG_BINARY)
			{
				m_errorCode = Error::INCORRECT_TYPE;
				return false;
			}

			result.resize(size);

			m_errorCode = tryQuery(valueName, NULL, &result[0], &size);

			if (m_errorCode != Error::OK)
				return false;

			close();

			return true;
		}

		bool readDWORD(std::string valueName, DWORD & result)
		{
			m_errorCode = tryOpenRead();

			if (m_errorCode != Error::OK)
				return false;

			DWORD type;
			DWORD size = sizeof(DWORD);

			m_errorCode = tryQuery(valueName, &type, &result, &size);

			if (m_errorCode != Error::OK)
				return false;

			if (type != REG_DWORD)
			{
				m_errorCode = Error::INCORRECT_TYPE;
				return false;
			}

			close();

			return true;
		}

		bool readQWORD(std::string valueName, QWORD & result)
		{
			m_errorCode = tryOpenRead();

			if (m_errorCode != Error::OK)
				return false;

			DWORD type;
			DWORD size = sizeof(QWORD);

			m_errorCode = tryQuery(valueName, &type, &result, &size);

			if (m_errorCode != Error::OK)
				return false;

			if (type != REG_QWORD)
			{
				m_errorCode = Error::INCORRECT_TYPE;
				return false;
			}

			close();

			return false;
		}

		bool readSZ(std::string valueName, std::string & result)
		{
			m_errorCode = tryOpenRead();

			if (m_errorCode != Error::OK)
				return false;

			DWORD type;
			DWORD size;

			m_errorCode = tryQuery(valueName, &type, NULL, &size);

			if (m_errorCode != Error::OK)
				return false;

			if (type != REG_SZ)
			{
				m_errorCode = Error::INCORRECT_TYPE;
				return false;
			}

			result.resize(size);

			m_errorCode = tryQuery(valueName, NULL, &result[0], &size);

			if (m_errorCode != Error::OK)
				return false;

			result.resize(--size);
			close();

			return true;
		}

		bool readExpandSZ(std::string valueName, std::string & result)
		{
			m_errorCode = tryOpenRead();

			if (m_errorCode != Error::OK)
				return false;

			DWORD type;
			DWORD size;

			m_errorCode = tryQuery(valueName, &type, NULL, &size);

			if (m_errorCode != Error::OK)
				return false;

			if (type != REG_EXPAND_SZ)
			{
				m_errorCode = Error::INCORRECT_TYPE;
				return false;
			}

			std::string tmp;
			tmp.resize(size);

			m_errorCode = tryQuery(valueName, NULL, &tmp[0], &size);

			if (m_errorCode != Error::OK)
				return false;

			if (!(size = ExpandEnvironmentStringsA(&tmp[0], NULL, 0)))
			{
				m_errorCode = Error::CANNOT_READ_EXPAND;
				return false;
			}

			result.resize(size);

			if (!ExpandEnvironmentStringsA(&tmp[0], &result[0], size))
			{
				m_errorCode = Error::CANNOT_READ_EXPAND;
				return false;
			}

			result.resize(size -= 2);
			close();

			return true;
		}

		bool readMultiSZ(std::string valueName, std::vector<std::string> & result)
		{
			m_errorCode = tryOpenRead();

			if (m_errorCode != Error::OK)
				return false;

			DWORD type;
			DWORD size;

			m_errorCode = tryQuery(valueName, &type, NULL, &size);

			if (m_errorCode != Error::OK)
				return false;

			if (type != REG_MULTI_SZ)
			{
				m_errorCode = Error::INCORRECT_TYPE;
				return false;
			}

			std::string tmp;
			tmp.resize(size);

			m_errorCode = tryQuery(valueName, NULL, &tmp[0], &size);

			if (m_errorCode != Error::OK)
				return false;

			unsigned begin = 0;
			unsigned end = -1;

			while ((end = tmp.find_first_of('\0', end + 1)) != std::string::npos)
			{
				result.push_back(tmp.substr(begin, end - begin));
				begin = end + 1;
			}

			return true;
		}
	};
}