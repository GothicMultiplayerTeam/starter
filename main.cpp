#include <string>

#define NOMINMAX

#include "ServerRequest.h"
#include "Proxy.h"
#include "Registry.h"

#include <console-color.hpp>
#include <argparse/argparse.hpp>

enum ExitCode
{
    OK,
    PARSING_ARGS_FAILED,
    INVALID_IP_PORT,
    REQUEST_INIT_FAILED,
    REQUEST_TIMEOUT_REACHED,
    SERVER_IS_FULL,
    RUN_FAILED,
};

void info(std::string message)
{
    std::cout << console::color_scoped("[info] ", console::shade::grey) << message << "\n";
}

// use only while exiting
void warning(std::string message)
{
    std::cerr << console::color_scoped("[warning] ", console::shade::yellow) << message << "\n";
}

// use only while exiting
void error(std::string message)
{
    std::cerr << console::color_scoped("[error] ", console::shade::red) << message << "\n";
}

bool sendRequest(ServerAddress& address, ServerRequest& request, Packet* packet)
{
    info("Trying to connect with: " + address.ip + ":" + std::to_string(address.port));

    if  (
            (dynamic_cast<PacketPing*>(packet) && !request.ping(address, *packet)) ||
            (dynamic_cast<PacketInfo*>(packet) && !request.info(address, *packet)) ||
            (dynamic_cast<PacketDetails*>(packet) && !request.details(address, *packet))
        )
    {
        error("Connection timeout " + std::to_string(request.timeout) + " miliseconds reached.");
        return false;
    }

    info("Got response from the server");
    return true;
}

/*
    G2O_Starter.exe --connect ip:port [--timeout 5] [--nickname nickname --args "gothic2.exe params"]
    G2O_Starter.exe --ping ip:port [--timeout 5]
    G2O_Starter.exe --details ip:port [--timeout 5]
    G2O_Starter.exe --info ip:port [--timeout 5]
*/

int main(int argc, char* argv[])
{
    argparse::ArgumentParser program("G2O_Starter");

    program.add_argument("--connect")
        .help("Uses ip:port string to connect with the server");

    program.add_argument("--nickname")
        .help("Specifies the nickname of the player");

    program.add_argument("--ping")
        .help("Gets the ping between server and the client");

    program.add_argument("--details")
        .help("Gets the server details");

    program.add_argument("--info")
        .help("Gets the server info");

    program.add_argument("--timeout")
        .help("Specifies the request timeout in seconds")
        .default_value(5)
        .scan<'i', int>();

    try {
        program.parse_args(argc, argv);
    }
    catch (const std::runtime_error& err) {
        std::cerr << err.what() << std::endl;
        std::cerr << program;
       
        return PARSING_ARGS_FAILED;
    }

    std::string nickname = "";
    std::string ip_port = "";

    if (program.is_used("--connect"))
    {
        ip_port = program.get<std::string>("--connect");

        if (program.is_used("--nickname"))
            nickname = program.get<std::string>("--nickname");
    }
    else if (program.is_used("--ping"))
        ip_port = program.get<std::string>("--ping");
    else if (program.is_used("--details"))
        ip_port = program.get<std::string>("--details");
    else if (program.is_used("--info"))
        ip_port = program.get<std::string>("--info");

    if (program.is_used("--connect"))
    {
        ServerAddress serverAddress;

        if (!ServerAddress::parse(ip_port, serverAddress))
        {
            error("Invalid ip:port");
            return INVALID_IP_PORT;
        }

        ServerRequest serverRequest;

        if (!serverRequest.init())
        {
            error(serverRequest.getErrorDescription());
            return REQUEST_INIT_FAILED;
        }

        if (program.is_used("--timeout"))
            serverRequest.timeout = program.get<int>("--timeout") * 1000;

        // # Getting server info
        PacketInfo packetInfo;

        if (!sendRequest(serverAddress, serverRequest, &packetInfo))
            return REQUEST_TIMEOUT_REACHED;

        if (packetInfo.players == packetInfo.maxSlots)
        {
            warning("Cannot join the server, because it's full!");
            return SERVER_IS_FULL;
        }

        // # Writing g2o values to registry
        Registry::Key key(HKEY_CURRENT_USER, "SOFTWARE\\G2O");

        if
        (
            (ip_port != "" && !key.writeSZ("ip_port", ip_port)) ||
            (nickname != "" && !key.writeSZ("nickname", nickname))
        )
            info(key.getErrorDescription() + ". Try running starter as administrator next time.\n\tGame will start with default values.");

        // # Running g2o
        info("Starting game...");

        Proxy::Error result = Proxy::run(packetInfo.version);

        if (result != Proxy::Error::OK)
        {
            error(Proxy::getErrorDescription());
            return RUN_FAILED;
        }
    }
    else if (program.is_used("--ping"))
    {
        ServerAddress serverAddress;

        if (!ServerAddress::parse(ip_port, serverAddress))
        {
            error("Invalid ip:port");
            return INVALID_IP_PORT;
        }

        ServerRequest serverRequest;

        if (!serverRequest.init())
        {
            error(serverRequest.getErrorDescription());
            return REQUEST_INIT_FAILED;
        }

        if (program.is_used("--timeout"))
            serverRequest.timeout = program.get<int>("--timeout") * 1000;

        PacketPing packetPing;

        if (!sendRequest(serverAddress, serverRequest, &packetPing))
            return REQUEST_TIMEOUT_REACHED;

        info("ping: " + std::to_string(packetPing.ping));
    }
    else if (program.is_used("--details"))
    {
        ServerAddress serverAddress;

        if (!ServerAddress::parse(ip_port, serverAddress))
        {
            error("Invalid ip:port");
            return INVALID_IP_PORT;
        }

        ServerRequest serverRequest;

        if (!serverRequest.init())
        {
            error(serverRequest.getErrorDescription());
            return REQUEST_INIT_FAILED;
        }

        if (program.is_used("--timeout"))
            serverRequest.timeout = program.get<int>("--timeout") * 1000;

        PacketDetails packetDetails;

        if (!sendRequest(serverAddress, serverRequest, &packetDetails))
            return REQUEST_TIMEOUT_REACHED;

        std::cout << "\n-========================================-\n";
        std::cout << "-= " << console::color_scoped("Server details", console::shade::bright_cyan) << "\n";
        std::cout << "-========================================-\n";
        std::cout << console::color_scoped("description: ", console::shade::bright_yellow) << packetDetails.description << "\n";
        std::cout << console::color_scoped("world: ", console::shade::bright_yellow) << packetDetails.world << "\n";
        std::cout << "-========================================-\n";
    }
    else if (program.is_used("--info"))
    {
        ServerAddress serverAddress;

        if (!ServerAddress::parse(ip_port, serverAddress))
        {
            error("Invalid ip:port");
            return INVALID_IP_PORT;
        }

        ServerRequest serverRequest;

        if (!serverRequest.init())
        {
            error(serverRequest.getErrorDescription());
            return REQUEST_INIT_FAILED;
        }

        if (program.is_used("--timeout"))
            serverRequest.timeout = program.get<int>("--timeout") * 1000;

        PacketInfo packetInfo;

        if (!sendRequest(serverAddress, serverRequest, &packetInfo))
            return REQUEST_TIMEOUT_REACHED;

        std::cout << "\n-========================================-\n";
        std::cout << "-= " << console::color_scoped("Server info", console::shade::bright_cyan) << "\n";
        std::cout << "-========================================-\n";
        std::cout << console::color_scoped("hostname: ", console::shade::bright_yellow) << packetInfo.hostName << "\n";
        std::cout << console::color_scoped("ping: ", console::shade::bright_yellow) << packetInfo.ping << "\n";
        std::cout << console::color_scoped("version: ", console::shade::bright_yellow) << packetInfo.version.major << "." << packetInfo.version.minor << "." << packetInfo.version.patch << "\n";
        std::cout << console::color_scoped("codeName: ", console::shade::bright_yellow) << packetInfo.codeName << "\n";
        std::cout << console::color_scoped("players: ", console::shade::bright_yellow) << packetInfo.players << "\n";
        std::cout << console::color_scoped("maxSlots: ", console::shade::bright_yellow) << packetInfo.maxSlots << "\n";
        std::cout << "-========================================-\n";
    }
    else
        std::cout << program;

	return OK;
}