#include "ServerAddress.h"
#include <iostream>

bool ServerAddress::parse(std::string ip_port, ServerAddress& address)
{
    int colonPos = ip_port.find(':');

    try
    {
        address.ip = ip_port.substr(0, colonPos);
        address.port = stoi(ip_port.substr(colonPos + 1));

        return true;
    }
    catch (std::invalid_argument) {}

    return false;
}

bool ServerAddress::isUnassigned()
{
    return ip.size() == 0;
}
