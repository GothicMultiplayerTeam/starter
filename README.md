# Introduction

The starter is a simple command line utility for querying server information and connecting with the server.

# Usage

To use the starter, you first need to:

- open the `powershell` as administrator
- `cd` into folder where starter is located

You can find full list commands by typing: `./G2O_Starter --help`

### Examples

#### Connecting with the server
- `./G2O_Starter --connect 127.0.0.1:28970`

#### Pinging server
- `./G2O_Starter --ping 127.0.0.1:28970`

#### Getting server info (hostname, ping, version, codeNam, players, maxSlots, etc.)
- `./G2O_Starter --info 127.0.0.1:28970`

#### Getting server details (description, world, etc.)
- `./G2O_Starter --details 127.0.0.1:28970`

### Optional flags
- `--nickname name`  
	allows you to set custom nickname while connecting with the server

- `--timeout seconds`  
	allows you to set custom timeout after which request will fail