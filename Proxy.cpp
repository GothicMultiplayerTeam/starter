#include <Windows.h>
#include <string>
#include "Proxy.h"

struct RunResult
{
    Proxy::Error result;
    int error;
};

typedef Version(*FUNC_Version)();
typedef RunResult(*FUNC_Run)(int, int, int);

std::string errorDescription;

const char* possibleSolution(int code)
{
    switch (code)
    {
    case 5:
        return "http://dl1.worldofplayers.ru/games/gothic/patches/G2/gothic2_fix-2.6.0.0-rev2.exe \nGothic Fix 2.6-rev (fix) is not installed";

    case 126:
        return "Missing https://support.microsoft.com/pl-pl/help/2999226/update-for-universal-c-runtime-in-windows \nC Runtime Library";

    case 193:
        return "https://www.microsoft.com/pl-pl/download/details.aspx?id=48145 Visual Redistribute Patch 2015 (x86) is not installed."
            "But if you already installed it, use \"Repair\" option.";
    }

    return "Unknown";
}

Version Proxy::version()
{
    HMODULE hProxy = LoadLibraryA("G2O_Proxy.dll");
    if (hProxy == nullptr)
    {
        errorDescription = "Cannot load G2O_Proxy.dll!\n";
        errorDescription += "\tError code: ";
        errorDescription += std::to_string(GetLastError());

        return Version({ 0, 0, 0, 0 });
    }

    FUNC_Version funcVersion = (FUNC_Version)GetProcAddress(hProxy, "G2O_Version");
    if (funcVersion == nullptr)
    {
        errorDescription = "Cannot find function G2O_Version in G2O_Proxy.dll!\n";
        errorDescription += "\tError code: ";
        errorDescription += std::to_string(GetLastError());

        return Version({ 0, 0, 0, 0 });
    }

    Version version = funcVersion();
    FreeLibrary(hProxy);

    return version;
}

Proxy::Error Proxy::run(Version version)
{
    HMODULE hProxy = LoadLibraryA("G2O_Proxy.dll");
    if (hProxy == nullptr)
    {
        errorDescription = "Cannot load G2O_Proxy.dll!\n";
        errorDescription += "\tError code : ";
        errorDescription += std::to_string(GetLastError());

        return Proxy::Error::UNKNOWN;
    }

    FUNC_Run funcRun = (FUNC_Run)GetProcAddress(hProxy, "G2O_Run");
    if (funcRun == nullptr)
    {
        errorDescription = "Cannot find function G2O_Run in G2O_Proxy.dll!\n";
        errorDescription += "\tError code: ";
        errorDescription += std::to_string(GetLastError());

        return Proxy::Error::UNKNOWN;
    }

    RunResult result = funcRun(version.major, version.minor, version.patch);

    if (result.result != Proxy::Error::OK)
    {
        switch (result.result)
        {
            case Proxy::Error::MISSING_VERSION:
                errorDescription = "You can't join to this server, because you don't have required version!\n";
                errorDescription += "\tError code : " + result.error;
                break;

            case Proxy::Error::GOTHIC_NOT_FOUND:
                errorDescription = "Could not open Gothic2.exe.\n";
                errorDescription += "\tDid you install G2O to, a folder with Gothic 2: Night of the Raven ?\n";
                errorDescription += "\tTry to run launcher with admin rights.\n";
                errorDescription += "\tError code : " + result.error;
                break;

            case Proxy::Error::UNKNOWN:
                errorDescription = "Unknown error.\n";
                errorDescription += "\tPossible solution : ";
                errorDescription += possibleSolution(result.error);
                errorDescription += "\n";
                errorDescription += "\tError code: ";
                errorDescription += result.error;
                break;
        };
    }

    return result.result;
}

std::string Proxy::getErrorDescription()
{
    return errorDescription;
}
