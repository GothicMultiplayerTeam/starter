#ifndef PROXY_H
#define PROXY_H

#include "version.h"

namespace Proxy
{
    enum class Error
    {
        OK,
        MISSING_VERSION,
        GOTHIC_NOT_FOUND,
        UNKNOWN
    };

    Version version();
    Error run(Version version);

    std::string getErrorDescription();
}

#endif